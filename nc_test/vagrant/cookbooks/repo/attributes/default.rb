# EPEL
default['epel']['file_name']  = "epel-release-6-8.noarch.rpm"
default['epel']['remote_uri'] = "ftp://ftp.kddilabs.jp/Linux/distributions/fedora/epel/6/x86_64/epel-release-6-8.noarch.rpm"
#default['epel']['remote_uri'] = "http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm"

# remi
default['remi']['file_name']  = "remi-release-6.rpm"
default['remi']['remote_uri'] = "http://rpms.famillecollet.com/enterprise/remi-release-6.rpm"
# RPMForge
default['rpmforge']['file_name']  = "rpmforge-release-0.5.3-1.el6.rf.i686.rpm"
default['rpmforge']['remote_uri'] = "http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el6.rf.i686.rpm"
