#
# Cookbook Name:: php54
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# php.ini設定
template "php.ini" do
  path "/etc/php.ini"
  source "php.ini.erb"
  mode 0644
  action :nothing
end

# PHPサンプル
template "phpinfo.php" do
  path "/var/www/html/phpinfo.php"
  source "phpinfo.php.erb"
  mode 0644
  action :nothing
end

# パッケージインストール
package "php" do
  action :install
  options "--enablerepo=remi"
  notifies :create, resources( :template => "php.ini" )
  notifies :create, resources( :template => "phpinfo.php" )
  notifies :restart, 'service[httpd]'
end

# 関連パッケージのインストール
%w{
  php-mysql
  php-gd
  php-pdo
  php-pear
  php-mbstring
  php-devel
}.each do |p|
  package p do
    action :install
    options "--enablerepo=remi"
  end
end

# php-mcryptパッケージのインストール
package "php-mcrypt" do
  action :install
  options "--enablerepo=epel,remi,rpmforge"
end