#
# Cookbook Name:: composer
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

execute "composer install" do
  command <<-EOS
    curl -sS #{node['composer']['url']} | php -- --install-dir=#{node['composer']['install_dir']}
    ln -s #{node['composer']['install_dir']}composer.phar #{node['composer']['bin']}
    
    cd /vagrant/
    composer install
  EOS
end