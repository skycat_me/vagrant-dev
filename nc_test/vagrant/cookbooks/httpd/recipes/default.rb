#
# Cookbook Name:: httpd
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# httpd.conf設定
template "project.conf" do
  path "/etc/httpd/conf.d/project.conf"
  source "project.conf.erb"
  mode 0644
  action :nothing
end

# パッケージインストール
package "httpd" do
  action :install
  notifies :create, resources( :template => "project.conf" )
end

# サービス起動と自動起動設定
service "httpd" do
  action [:start, :enable]
end

# firewallを切る
service "iptables" do
  action [:stop, :enable]
end

