#
# Cookbook Name:: phpmyadmin
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


#

link "/src/public_html/phpmyadmin" do
  to "/usr/share/phpMyAdmin"
   link_type :symbolic
end


# phpmyadmin.conf設定
template "phpmyadmin.conf" do
  path "/etc/httpd/conf.d/phpmyadmin.conf"
  source "phpmyadmin.conf.erb"
  mode 0644
  action :nothing
end


# パッケージインストール
package "phpMyAdmin" do
  action :install
  options "--enablerepo=remi"
end

