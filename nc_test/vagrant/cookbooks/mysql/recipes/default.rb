#
# Cookbook Name:: mysql
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# my.cnf設定
template "my.cnf" do
  path "/etc/my.cnf"
  source "my.cnf.erb"
  mode 0644
  action :nothing
end

# パッケージインストール
package "mysql-server" do
  action :install
  options "--enablerepo=remi"
  notifies :create, resources( :template => "my.cnf" )
end

# サービス起動と自動起動設定
service "mysqld" do
  action [:start, :enable]
end

# セキュリティ設定
# 匿名ユーザーの削除
# リモートからのrootユーザー接続を禁止
# testデータベースの削除
# rootユーザーパスワードの設定
# 権限データベースのリロード
script "secure_setting" do
  interpreter 'bash'
  user "root"
  not_if "mysql -u root -p#{node[:mysql][:root_passwd]} -e 'show status'"
  code <<-EOC
mysql -u root -e "CREATE DATABASE #{node[:mysql][:new_db]};"
mysql -u root -e "DELETE FROM mysql.user WHERE User='';"
mysql -u root -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
mysql -u root -e "GRANT ALL ON *.* TO #{node[:mysql][:new_user]}@'#{node[:mysql][:new_host]}' IDENTIFIED BY '#{node[:mysql][:new_passwd]}';"
mysql -u root -e "UPDATE mysql.user SET Password=PASSWORD('#{node[:mysql][:root_passwd]}') WHERE User='root';"
mysql -u root -e "FLUSH PRIVILEGES;"
  EOC
end
